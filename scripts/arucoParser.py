import cv2
import cv2.aruco as aruco
import numpy as np
import os

def findArucoMarkers(img, makrkersize=6, totalMarkers=250, draw=True):
    imgGray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    arucoDict = aruco.Dictionary_get(aruco.DICT_4X4_250)
    arucoParam = aruco.DetectorParameters_create()
    bboxs, ids, rej = aruco.detectMarkers(imgGray, 
                                          arucoDict, parameters=arucoParam)
    print(ids, bboxs)

    return ids, bboxs
   
    
	

def main():
    cap = cv2.VideoCapture("/dev/video0")
    # cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)
    # cap.set(cv2.CAP_PROP_FRAME_HEIGHT,1080)
    while True:
        sccuess, img = cap.read()
        # img = cv2.imread("test2.jpg")
        ids, bboxs = findArucoMarkers(img)

        aruco.drawDetectedMarkers(img, bboxs)
        
        if (ids is not None and len(ids) > 0):
            for i in range(len(ids)):
                cv2.putText(img, str(ids[i]), (int((bboxs[i][0][0][0] + bboxs[i][0][2][0])/2), int((bboxs[i][0][0][1]+ bboxs[i][0][2][1]) / 2)), cv2.FONT_HERSHEY_SIMPLEX, 2, (0, 0, 255), 2)
                # print(ids[i], "DONE")
      
        cv2.imshow("Image", img)
        cv2.waitKey(60)

if __name__ == "__main__":
    main()
