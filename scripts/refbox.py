import os
import json
import random
import numpy as np
import collections

class Refbox:
    
    def __init__ (self, config):
        self.cargo_count = int(config['cargo_count'])
        self.max_cargo_count = config["max_cargo_count"]
        self.loading_zones = config["loading_zones"]
        self.loading_zones_count = len(self.loading_zones)
        self.storage_count = config["storage_count"]
        self.robot_count = config["robot_count"]
        self.state = [-1] * self.cargo_count
        self.orders_count_interval = config["orders_count_interval"]
        self.global_task = []
        self.id = 101
        self.current_orders = []
        self.future_state = [-1] * self.cargo_count
        self.used_cargos = [0]*self.cargo_count
        self.last_id = 100
        

    def generateInitInfo(self):
        
        for i in range(self.cargo_count):
            storage_place = -1
            while storage_place in self.state and storage_place < self.storage_count:
                storage_place = random.randrange(self.storage_count + 20*self.loading_zones_count)
                if storage_place >= self.storage_count:
                    idx = (storage_place - self.storage_count) % self.loading_zones_count
                    storage_place = self.loading_zones[idx]
                    counter = collections.Counter(self.state)
                    while (counter[storage_place] >= self.max_cargo_count and idx < self.loading_zones_count):
                        idx += 1
                       
                        if idx < self.loading_zones_count:
                            storage_place = self.loading_zones[idx]
                        
                       
                    if idx >= self.loading_zones_count:
                        storage_place = random.randrange(self.storage_count)
                    else:
                        break
            self.state[i] = storage_place
        self.future_state = self.state.copy()

    def generateTaskList(self):
        orders_count = random.randrange(self.orders_count_interval[0], self.orders_count_interval[1])
        while len(self.global_task) < orders_count:
            action_type = random.randrange(2)
            if action_type == 0: # to
                current_cargo = random.randrange(self.cargo_count)
                current_storage_place = self.state[current_cargo]
                storage_place_goal = random.choice(self.loading_zones)
                idx = self.loading_zones.index(storage_place_goal)
                counter = collections.Counter(self.future_state)
                while counter[storage_place_goal] >= self.max_cargo_count and idx < self.loading_zones_count:
                   
                    idx += 1
                    if idx < self.loading_zones_count:
                        storage_place_goal = self.loading_zones[idx]

                if idx >= self.loading_zones_count:
                    continue

                while (storage_place_goal == current_storage_place or self.used_cargos[current_cargo] == 1):
                    
                    current_cargo = random.randrange(self.cargo_count)
                    current_storage_place = self.state[current_cargo]
                    
                order = {
                    "task_id":self.id,
                    "cargo_id":current_cargo,
                    "sector":storage_place_goal,
                    "action":"to"
                }
                

                self.used_cargos[current_cargo] = 1
                self.future_state[current_cargo] = storage_place_goal
                self.id += 1
            if action_type == 1:
                
                storage_place_goal = random.choice(self.loading_zones)
                idx = self.loading_zones.index(storage_place_goal)
                state_buffer = self.future_state.copy()
                for i, cargo in enumerate(state_buffer):
                    if self.used_cargos[i] == 1:
                        state_buffer[i] = -1
                counter = collections.Counter(state_buffer)
                while counter[storage_place_goal] <= 0 and idx < self.loading_zones_count:
                    
                    idx += 1
                    if idx < self.loading_zones_count:
                        storage_place_goal = self.loading_zones[idx]

                if idx >= self.loading_zones_count:
                    continue
                current_cargo = -1
                for i, storage in enumerate(self.future_state):
                    if storage == storage_place_goal and self.used_cargos[i] == 0:
                        current_cargo = i
               
                order = {
                    "task_id":self.id,
                    "cargo_id":current_cargo,
                    "sector":storage_place_goal,
                    "action":"from"
                }
                self.used_cargos[current_cargo] = 1
                self.future_state[current_cargo] = random.randrange(self.storage_count)
                self.id += 1
            self.global_task.append(order)

    def getTask(self):
        return self.global_task

    def sendMessage(self, msg):
        # print(msg)
        try:
            msg = msg.replace("'", '"')
            # print(msg)
            msg = json.loads(msg)
            flag = False
            order_ = None
            for order in self.global_task:
                if msg["task_id"] == order["task_id"]:
                    flag = True
                    order_ = order
                    break

            if not flag:
                print("ERROR: TASK WITH THAT ID DOES NOT EXIST!")
                return
            # print(order_["status"], msg["action"])
            if order_["status"] == None:
                if msg["action"] != "reached":
                    print("ERROR: WRONG ACTION")
                else:
                    order_["status"] = "reached"
                return

            if order_["status"] == "reached":
                if msg["action"] != "grabbed":
                    print("ERROR: WRONG ACTION")
                else:
                    order_["status"] = "grabbed"
                return

            if order_["status"] == "grabbed": 
                if msg["action"] != "delivered":
                    print("ERROR: WRONG ACTION")  
                else:
                    order_["status"] = "delivered"
                return

            if order_["status"] == "delivered": 
                if msg["action"] != "put":
                    print("ERROR: WRONG ACTION") 
                else:
                    order_["status"] = "put"
                return   
            
            if order_["status"] == "put": 
                    print("ERROR: WRONG ACTION") 

        except:
            msg = str(msg)
            words = msg.split(" ")
            
            id = int(words[1])
            for order in self.global_task:
                if order["task_id"] == id:
                    if words[0] == "START":
                        if len(self.current_orders) + 1 > self.robot_count:
                            print("ERROR: ATTEMP TO START MORE TASKS THAN POSSIBLE!") 
                        if id - 1 != self.last_id:
                            print("ERROR: WRONG ORDER OF TASKS!") 

                        self.last_id = id
                        order["status"] = None
                        self.current_orders.append(order)
                    if words[0] == "END":
                        if order["status"] != "put":
                            print("ERROR: ORDER NOT FINISHED") 
                            return
                        
                        self.current_orders.remove(order)
                        self.state[order["cargo_id"]] = order["sector"]
                    if words[0] != "END" and words[0] != "START":
                        print("ERROR: WRONG COMAND!")
                    break
            print(self.current_orders)
