import json
from refbox import Refbox
from vizual import Vizual
import threading
import time
import sys
import cv2
import socket
import sys
import threading


with open("config.json") as file:
    config = json.load(file)
    refbox = Refbox(config)
    refbox.generateInitInfo()
    
print(refbox.state)
refbox.generateTaskList()

print(refbox.global_task)

vizualizator = Vizual(refbox)
vizualizator.createGraph()
img = cv2.imread("nto_sklad_v3.jpg")
img = vizualizator.showState(img)
cv2.imwrite("OriginState.jpg",img)


# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

server_address = ('localhost', 10000)
# print, 'starting up on %s port %s' % server_address
sock.bind(server_address)

sock.listen()

while True:
    # Wait for a connection
    print('waiting for a connection')
    connection, client_address = sock.accept()
    try:
        print('connection from', client_address)
        connection.sendall((str(refbox.getTask())).encode())
        # Receive the data in small chunks and retransmit it
        while True:
            data = (connection.recv(1024)).decode()
        
            if data != '':
                refbox.sendMessage(data)
            else:
                exit(0)
            
    finally:
        # Clean up the connection
        connection.close()
