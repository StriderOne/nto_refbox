import socket
import sys
import json
import time
# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Connect the socket to the port where the server is listening
server_address = ('localhost', 10000)
# Messages examples
msgs = ["START 101", "START 102", {"task_id":102, "action":"reached"}, {"task_id":102, "action":"reached"}, {"task_id":102, "action":"delivered"}, {"task_id":102, "action":"put"}, "END 102"]
sock.connect(server_address)

try:
    # get message from server
    data = (sock.recv(1024)).decode()
    # print task from refbox
    print('received', data)
    time.sleep(1)
    for message in msgs:
        # Send data
        print('sending', message)
        if isinstance(message, str):
            # if string
            sock.sendall(message.encode())
        else:
             # if dict
            user_encode_data = json.dumps(message, indent=2).encode()
            sock.sendall(user_encode_data)
        
        time.sleep(1)

finally:
    print('closing socket')
    sock.close()