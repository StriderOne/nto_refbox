from refbox import Refbox
import json
import cv2
import numpy as np
import math
import random

def dist(p1, p2):
    return math.sqrt((p1[0] - p2[0])**2 + (p1[1] - p2[1])**2)


class Vizual:
    def __init__(self, refbox: Refbox):
        self.refbox = refbox
        self.img = cv2.imread("nto_sklad_v3.jpg")
        self.storage_coordinates = [0] * self.refbox.storage_count
        self.vertex_coordinates = []
        self.graph = None
        self.findStorages()
        self.loading_zones_coorinates = [
                (1200, 580),
                (1080, 300)
        ]
        self.Acoordinates = [
            (1200, 380),
            (1200, 430),
            (1200, 480),
            (1200, 530),
            (1200, 580),
            (1200, 630),
            (1200, 680),
            (1200, 730)
        ]
        self.Apointer = 0
        self.Bcoordinates = [
            (905, 100),
            (905, 150),
            (905, 200),
            (905, 250),
            (905, 300),
            (960, 300),
            (1010, 300),
            (1060, 300)
        ]
        self.Bpointer = 0

    def showOriginImage(self):
        cv2.imshow("Origin image", self.img)
        cv2.waitKey(0)

    def findStorages(self):
        delta = 5
        color_storage_l = (179 - delta, 179 - delta, 179 - delta)
        color_storage_u = (179 + delta, 179 + delta, 179 + delta)

        img_gr = cv2.inRange(self.img, color_storage_l, color_storage_u)
       
        contours, hierarchy = cv2.findContours(img_gr, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
        image = np.zeros(self.img.shape)
        count = 0
        for cnt in contours:
            if len(cnt) < 100:
                continue
            M = cv2.moments(cnt)
            cX = int(M["m10"] / M["m00"])
            cY = int(M["m01"] / M["m00"])

            self.storage_coordinates[count] = (cX, cY)
            count += 1
    
    def showState(self, image):
        # image = self.img.copy()
        for i, pos in enumerate(self.refbox.state):
            # print(self.storage_coordinates[pos])
            if isinstance(pos, str):
                if pos == "A":
                    cv2.putText(image, str(i), (self.Acoordinates[self.Apointer][0] - 15, self.Acoordinates[self.Apointer][1] + 10), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255))
                    self.Apointer += 1
                if pos == "B":
                    cv2.putText(image, str(i), (self.Bcoordinates[self.Bpointer][0] - 10, self.Bcoordinates[self.Bpointer][1] + 10), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255))
                    self.Bpointer += 1
                continue
            if (len(str(i)) < 2):
                cv2.putText(image, str(i), (self.storage_coordinates[pos][0] - 5, self.storage_coordinates[pos][1] + 10), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255))
            else:
                cv2.putText(image, str(i), (self.storage_coordinates[pos][0] - 20, self.storage_coordinates[pos][1] + 10), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255))

        # cv2.imshow("test", image)
        return image
        

    def createGraph(self):
        image = self.img.copy()
        kernel = np.ones((5, 5), 'uint8')
        delta = 10
        er_image = cv2.erode(image, kernel, iterations=11)
        vertex_count = 0
        for x in range(10, 1350, delta):
            for y in range(10, 810, delta):
                if er_image[y][x][0] >= 253 or (er_image[y][x][2] < 220 and er_image[y][x][2] > 162 and er_image[y][x][0] > 162 and er_image[y][x][0] < 180):
                    self.vertex_coordinates.append((x,y))
                    cv2.circle(image, (x,y), 2, (255, 0, 0), -1)
                    vertex_count += 1 
        
        self.graph = [[]] * (vertex_count + len(self.storage_coordinates) + self.refbox.loading_zones_count)
        
        for i, vertex in enumerate(self.vertex_coordinates):
                
                x = vertex[0]
                y = vertex[1]
                neighbours = []
                if list(image[y - delta][x]) == list((255, 0, 0)):
                    neighbours.append(self.vertex_coordinates.index((x, y - delta)))
                    
                if list(image[y + delta][x]) == list((255, 0, 0)):
                   neighbours.append(self.vertex_coordinates.index((x, y + delta)))
                
                if list(image[y][x + delta]) == list((255, 0, 0)):
                    neighbours.append(self.vertex_coordinates.index((x + delta, y)))
                    
                if list(image[y][x - delta]) == list((255, 0, 0)):
                    neighbours.append(self.vertex_coordinates.index((x - delta, y)))
                
                self.graph[i] = neighbours
        
        for i, storage in enumerate(self.storage_coordinates):
            min_ = 100000000
            neighbour = -1
            for j, vertex in enumerate(self.vertex_coordinates):
                if dist(storage, vertex) < min_:
                    min_ = dist(storage, vertex)
                    neighbour = j
            
            self.graph[i + len(self.vertex_coordinates)] = [neighbour]
            self.graph[neighbour].append(i + len(self.vertex_coordinates))
            
        
        for i, storage in enumerate(self.storage_coordinates):
            self.vertex_coordinates.append(storage)

        for i, storage in enumerate(self.loading_zones_coorinates):
            min_ = 100000000
            neighbour = -1
            for j, vertex in enumerate(self.vertex_coordinates):
                # print(storage, vertex)
                if dist(storage, vertex) < min_:
                    min_ = dist(storage, vertex)
                    neighbour = j
            self.graph[i + len(self.vertex_coordinates)] = [neighbour]
            self.graph[neighbour].append(i + len(self.vertex_coordinates))

        for i, storage in enumerate(self.loading_zones_coorinates):
            self.vertex_coordinates.append(storage)



    def showGraph(self):
        image = self.img.copy()
        for i in range(len(self.graph)):
            # print(i, "/", len(self.graph), len(self.graph[i]))
            if self.vertex_coordinates[i] in self.storage_coordinates:
                cv2.circle(image, self.vertex_coordinates[i], 3, (0, 255, 0), -1)
            else:
                cv2.circle(image, self.vertex_coordinates[i], 3, (255, 0, 0), -1)
            for j in self.graph[i]:
                cv2.line(image, self.vertex_coordinates[i], self.vertex_coordinates[j], (0, 0, 255), 1)

        cv2.imshow("test", image)
        cv2.waitKey(0)

    def findWay(self, start, finish):
        start += len(self.vertex_coordinates) - len(self.storage_coordinates) - self.refbox.loading_zones_count
        
        # print(self.vertex_coordinates[start], self.vertex_coordinates[finish])
        d = []
        p = []
        used = []
        inf = 100000000
        n = len(self.graph)
        for i in range(n):
            d.append(inf)
            p.append(-1)
            used.append(False)
        
        d[start] = 0

        for i in range(n):
            v = -1
            for j in range(n):
                if used[j] == False and (v == -1 or d[j] < d[v]):
                    v = j
            if (d[v] == inf):
                break
            used[v] = True

            for j, point in enumerate(self.graph[v]):
                to = self.graph[v][j]
                l = dist(self.vertex_coordinates[v], self.vertex_coordinates[to])
                if d[v] + l < d[to]:
                    d[to] = d[v] + l
                    p[to] = v
        
        path = []
        v = finish
        while v != start:
            # print(v) 
            point = [self.vertex_coordinates[v][0], self.vertex_coordinates[v][1]]
            path.append(point)
            v = p[v]

        path.reverse()
        return path

    def showCurrentOrders(self):
        
        delta = 4
        image = self.img.copy()
        
        for orders in self.refbox.current_orders:
            if orders["action"] == "from":
                break
            finish = orders["sector"]
        
            finish = len(self.vertex_coordinates) + self.refbox.loading_zones.index(finish) - self.refbox.loading_zones_count
            start =  self.refbox.state[orders["cargo_id"]]
            path = self.findWay(start, finish)
            for i, point in enumerate(path):
                if i == 0:
                    continue
                p1 = point
                p2 = path[i - 1]
                if (p1[0] == p2[0]):
                    if (p1[1] > p2[1]):
                        p1[1] = p1[1] - delta
                        p2[1] = p2[1] + delta
                    else:
                        p1[1] = p1[1] + delta
                        p2[1] = p2[1] - delta

                if (p1[1] == p2[1]):
                    if (p1[0] > p2[0]):
                        p1[0] = p1[0] - delta
                        p2[0] = p2[0] + delta
                    else:
                        p1[0] = p1[0] + delta
                        p2[0] = p2[0] - delta
                # color = list(np.random.choice(range(256), size=3))
                cv2.line(image, tuple(p1), tuple(p2), (255, 0, 0), 2)
        image = self.showState(image)
        cv2.imshow("test2", image)
        # cv2.waitKey(10)

